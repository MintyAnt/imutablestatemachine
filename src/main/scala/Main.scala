import main.scala.engine.Engine

/**
 * Created by duanese1 on 12/18/14
 */
object Main {
  def main(args: Array[String]): Unit = {
    val engine: Engine = Engine()
    engine.run()
  }
}
