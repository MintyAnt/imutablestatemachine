package main.scala.game.map

import main.scala.game.Entity.Entity

/**
 * Created by duanese1 on 12/18/14
 */
case class Map(entities: List[Entity]) {

  def update: Map = {
    // Update entities
    val updatedEntities: List[Entity] = entities.map((entity: Entity) => {
      val updatedEntity: Entity = entity.update
      updatedEntity.stateMachine.update(updatedEntity)
    })

    Map(updatedEntities)
  }
}
