package main.scala.game

import main.scala.game.Entity._
import main.scala.game.map.Map

/**
 * Created by duanese1 on 12/18/14
 */
object Game {
  def generateNewGame(): Game = {
    val minerEntity: Entity = Miner(StateMachine(Some(State_Miner_EnterMineAndDigForNugget())),
      Location.House, 0, 0, 0, 0)

    val newMap: Map = Map( List[Entity](minerEntity) )
    Game(newMap)
  }
}

case class Game(map: Map) {
  def update: Game = {
    val updatedMap: Map = map.update
    Game(updatedMap)
  }
}
