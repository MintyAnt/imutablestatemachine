package main.scala.game.Entity

/**
 * Created by duanese1 on 1/5/15
 */
case class StateMachine(currentState: Option[State]) {

  def update(owner: Entity): Entity = {
    val (updatedEntity: Entity, updatedState: Option[State]) = if (currentState.isDefined) {
      currentState.get.update(owner)
    } else (owner, currentState)

    if (currentState.isDefined && (!updatedState.isDefined || updatedState.get != currentState)) {
      currentState.get.exit(owner)
    }

    if (updatedState.isDefined && updatedState.get != currentState) {
      updatedState.get.enter(owner)
    }

    updatedEntity.copyFromBase(StateMachine(updatedState))
  }
}
