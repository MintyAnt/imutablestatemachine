package main.scala.game.Entity

/**
 * Created by duanese1 on 12/18/14
 */

object Location extends Enumeration {
  type Location = Value
  val Goldmine, House, Saloon = Value
}
import Location._

abstract class Entity() {
  val stateMachine: StateMachine = StateMachine(None)

  def copyFromBase(inStateMachine: StateMachine): Entity

  def update: Entity
}

case class Miner(override val stateMachine: StateMachine, location: Location, goldCarried: Int, moneyInBank: Int, thirst: Int, fatigue: Int) extends Entity {
  def isThirsty: Boolean = thirst >= 20

  def pocketsFull: Boolean = goldCarried >= 50

  override def update: Entity = {
    val newThirst = thirst + 1

    this.copy(thirst = newThirst)
  }

  def copyFromBase(inStateMachine: StateMachine): Entity = copy(stateMachine = inStateMachine)

}