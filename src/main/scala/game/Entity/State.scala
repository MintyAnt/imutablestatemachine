package main.scala.game.Entity

import main.scala.game.Entity.Location.Location

/**
 * Created by duanese1 on 12/18/14
 */
trait State {
  def enter(owner: Entity): Entity
  def update(owner: Entity): (Entity, Option[State])
  def exit(owner: Entity): Entity
}

case class State_Miner_EnterMineAndDigForNugget() extends State {
  override def enter(owner: Entity): Entity = {
    val miner: Miner = owner.asInstanceOf[Miner]

    if (miner.location != Location.Goldmine) {
      println("Walkin' to the gold mine")
    }
    val goldmineLocation: Location = Location.Goldmine

    val updatedMiner: Miner = miner.copy(location = goldmineLocation)
    updatedMiner
  }

  override def update(owner: Entity): (Entity, Option[State]) = {
    val miner: Miner = owner.asInstanceOf[Miner]

    // Mine 1 gold
    val gold: Int = miner.goldCarried + 1
    val fatigue: Int = miner.fatigue + 1

    println("Pickin' up a nugget")

    val updatedState: Option[State] = if (miner.pocketsFull) {
      // Switch to deposit state
      Some(this)
    } else if (miner.isThirsty) {
      // Switch to drink state
      Some(this)
    } else Some(this)

    val updatedMiner: Miner = miner.copy(goldCarried = gold, fatigue = fatigue)
    (updatedMiner, updatedState)
  }

  override def exit(owner: Entity): Entity = {
    println("Ah'm leavin' the gold mine with mah pockets full o' sweet gold")

    owner
  }
}